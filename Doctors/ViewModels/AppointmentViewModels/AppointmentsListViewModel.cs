﻿using Doctors.Models.Appointments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctors.ViewModels.AppointmentViewModels
{
    public class AppointmentsListViewModel
    {
        public IEnumerable<Appointment> Appointments { get; set; }
    }
}
