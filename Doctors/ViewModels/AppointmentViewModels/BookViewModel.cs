﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Doctors.ViewModels.AppointmentViewModels
{
    public class BookViewModel
    {
        public bool Success { get; set; }

        [DisplayName("Day")]
        public int DayId { get; set; }
        public SelectList Days { get; set; }

        [DisplayName("Doctor")]
        public string DoctorId { get; set; }

        [DisplayName("Patient")]
        public string PatientId { get; set; }

        public SelectList Patients { get; set; }

        public SelectList Doctors { get; set; }

        [DisplayName("Time")]
        public int TimesId { get; set; }
        public SelectList Times { get; set; }

        public bool IsBooked { get; set; }
    }
}
