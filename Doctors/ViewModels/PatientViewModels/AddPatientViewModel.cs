﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel;

namespace Doctors.ViewModels.PatientViewModels
{
    public class AddPatientViewModel
    {
        [DisplayName("Doctor")]
        public string DoctorId { get; set; }
        public SelectList Doctors { get; set; }
        public string Address { get; set; }
        public string Age { get; set; }
        public string Gender { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
    }
}
