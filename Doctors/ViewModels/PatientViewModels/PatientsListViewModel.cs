﻿using Doctors.Models.Patients;
using System.Collections.Generic;

namespace Doctors.ViewModels.PatientViewModels
{
    public class PatientsListViewModel
    {
        public IEnumerable<Patient> Patients { get; set; }
    }
}
