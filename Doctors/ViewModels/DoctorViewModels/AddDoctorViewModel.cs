﻿namespace Doctors.ViewModels.DoctorViewModels
{
    public class AddDoctorViewModel
    {
        public string Address { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
    }
}
