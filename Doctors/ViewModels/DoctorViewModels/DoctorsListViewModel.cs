﻿using Doctors.Models.Doctors;
using Doctors.Models.Patients;
using System.Collections.Generic;

namespace Doctors.ViewModels.DoctorViewModels
{
    public class DoctorsListViewModel
    {
        public IEnumerable<Patient> Patients { get; set; }
    }
}
