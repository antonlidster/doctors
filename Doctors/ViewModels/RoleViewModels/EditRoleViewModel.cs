﻿using System.ComponentModel.DataAnnotations;

namespace MVC.ViewModels.RoleViewModels
{
    public class EditRoleViewModel
    {
        [Required]
        public string Name { get; set; }
        public string NormalisedName { get; set; }
        public bool Success { get; set; }
    }
}
