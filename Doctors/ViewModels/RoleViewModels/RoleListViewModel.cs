﻿using Doctors.Models.Roles;
using System.Collections.Generic;

namespace MVC.ViewModels.RoleViewModels
{
    public class RoleListViewModel
    {
        public string Id { get; set; }
        public string RoleName { get; set; }
        public int NumberOfUsers { get; set; }

        public IEnumerable<Role> Roles { get; set; }
        public bool? Success { get; set; }
    }
}