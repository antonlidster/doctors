﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Doctors.ViewModels.DoctorToPatientViewModels
{
    public class AssignDoctorsToPatientsViewModel
    {
        public bool Success { get; set; }
        [DisplayName("Doctor")]
        public string DoctorId { get; set; }

        [DisplayName("Patient")]
        public string PatientId { get; set; }

        public SelectList Patients { get; set; }

        public SelectList Doctors { get; set; }
    }
}
