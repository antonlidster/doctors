﻿using Doctors.Models.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctors.ViewModels.UserViewModels
{
    public class UserListViewModel
    {
        public List<User> Users { get; set; }
    }
}
