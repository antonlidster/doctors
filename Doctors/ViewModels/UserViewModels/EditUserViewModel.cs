﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Doctors.ViewModels
{
    public class EditUserViewModel
    {
        [DisplayName("Username")]
        public string UserName { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [DisplayName("Phone number")]
        [Phone]
        public string PhoneNumber { get; set; }

        [DisplayName("Password")]
        public string PasswordHash { get; set; }
    }
}
