﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Doctors.ViewModels
{
    public class AddUserViewModel
    {
        [Required]
        [DisplayName("Username")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DisplayName("Phone number")]
        [Phone]
        public string PhoneNumber { get; set; }

        [Required]
        [DisplayName("Password")]
        public string PasswordHash { get; set; }
    }
}
