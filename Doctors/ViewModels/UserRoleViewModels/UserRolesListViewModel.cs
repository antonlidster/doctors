﻿using MVC.DTOs;
using System.Collections.Generic;

namespace Doctors.ViewModels.UserRoleViewModels
{
    public class UserRolesListViewModel
    {
        public List<UserRoleDto> UserRoles { get; set; }

        public bool? Success { get; set; }
    }
}
