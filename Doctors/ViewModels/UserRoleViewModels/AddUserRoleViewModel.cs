﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel;

namespace Doctors.ViewModels.UserRoleViewModels
{
    public class AddUserRoleViewModel
    {
        [DisplayName("User")]
        public string UserId { get; set; }

        [DisplayName("Role")]
        public string RoleId { get; set; }

        public SelectList Users { get; set; }

        public SelectList Roles { get; set; }

        public bool Success { get; set; }
    }
}
