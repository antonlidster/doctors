﻿using Doctors.Data;
using Doctors.Models.Appointments;
using Doctors.Models.Roles;
using Doctors.Models.Users;
using Microsoft.AspNetCore.Identity;
using MVC.Models.UserRoles;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace MVC
{
    public class Seed
    {
        public static void Initialize(ApplicationDbContext context)
        {
            context.Database.EnsureCreated();
            SeedData(context);
        }

        private static void SeedData(ApplicationDbContext context)
        {
            Users(context);
            Roles(context);
            UserRoles(context);
            Appointments(context);

        }

        private static void Appointments(ApplicationDbContext context)
        {
            IList<Appointment> appointments = new List<Appointment>();

            var appointmentsList = context.Appointments.ToList();

            string[] times = new string[] { "9", "10", "11", "12", "13", "14", "15", "16", "17" };

            DateTime startOfWeek = DateTime.Today.AddDays((int)CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek - (int)DateTime.Today.DayOfWeek);

            string result = string.Join("," + " ", Enumerable.Range(0, 7).Select(i => startOfWeek.AddDays(i).ToString("dd/MM/yyyy")));

            string[] currentWeeksDates = result.Split(',');

            foreach (var item in currentWeeksDates)
            {
                foreach (var t in times)
                {
                    appointments.Add(new Appointment()
                    {
                        Day = Convert.ToString(item),
                        Time = t,
                        IsBooked = false,

                    });
                }
         
            }

            if (!appointmentsList.Any())
            {
                foreach (Appointment a in appointments)
                {
                    context.Appointments.Add(a);
                    context.SaveChanges();
                }
            }

        }

        private static void UserRoles(ApplicationDbContext context)
        {
            IList<UserRole> userRoles = new List<UserRole>();

            var users = context.Users.ToList();

            foreach (var user in users)
            {
                var superAdminRole = context.Roles.FirstOrDefault(x => x.Name == "SuperAdmin");
                var adminRole = context.Roles.FirstOrDefault(x => x.Name == "Admin");
                var userRole = context.Roles.FirstOrDefault(x => x.Name == "User");

                switch (user.UserName)
                {
                    case "superadmin@base.com":
                        userRoles.Add(new UserRole()
                        {
                            UserId = user.Id,
                            RoleId = superAdminRole?.Id,

                        });
                        break;
                    case "admin@base.com":
                        userRoles.Add(new UserRole()
                        {
                            UserId = user.Id,
                            RoleId = adminRole?.Id,
                        });
                        break;
                    case "user@base.com":
                        userRoles.Add(new UserRole()
                        {
                            UserId = user.Id,
                            RoleId = userRole?.Id,
                        });
                        break;
                }
            }

            var userRolesList = context.UserRoles.ToList();

            if (!userRolesList.Any())
            {
                foreach (UserRole ur in userRoles)
                {
                    context.UserRoles.Add(ur);

                }

                context.SaveChanges();
            }

        }

        private static void Roles(ApplicationDbContext context)
        {
            IList<Role> roles = new List<Role>();

            roles.Add(new Role()
            {
                Name = "SuperAdmin",
                NormalizedName = "SuperAdmin".ToUpper()
            });

            roles.Add(new Role()
            {
                Name = "Admin",
                NormalizedName = "Admin".ToUpper()
            });


            roles.Add(new Role()
            {
                Name = "User",
                NormalizedName = "User".ToUpper()
            });

            var rolesList = context.Roles.ToList();

            if (!rolesList.Any())
            {
                foreach (Role r in roles)
                {
                    context.Roles.Add(r);
                }

                context.SaveChanges();
            }
        }

        private static IList<User> Users(ApplicationDbContext context)
        {
            User user = new User();

            IList<User> users = new List<User>();

            PasswordHasher<User> hasher = new PasswordHasher<User>();

            users.Add(new User()
            {
                UserName = "superadmin@base.com",
                Email = "superadmin@base.com",
                NormalizedEmail = "superadmin@base.com".ToUpper(),
                NormalizedUserName = "superadmin@base.com".ToUpper(),
                PasswordHash = hasher.HashPassword(user, "123qwe"),
                SecurityStamp = Guid.NewGuid().ToString(),

            });

            users.Add(new User()
            {
                UserName = "admin@base.com",
                Email = "admin@base.com",
                NormalizedEmail = "admin@base.com".ToUpper(),
                NormalizedUserName = "admin@base.com".ToUpper(),
                PasswordHash = hasher.HashPassword(user, "123qwe"),
                SecurityStamp = Guid.NewGuid().ToString(),
            });

            users.Add(new User()
            {
                UserName = "user@base.com",
                Email = "user@base.com",
                NormalizedEmail = "user@base.com".ToUpper(),
                NormalizedUserName = "user@base.com".ToUpper(),
                PasswordHash = hasher.HashPassword(user, "123qwe"),
                SecurityStamp = Guid.NewGuid().ToString(),

            });


            var usersList = context.Users.ToList();

            if (!usersList.Any())
            {
                foreach (User u in users)
                {
                    context.Users.Add(u);
                    context.SaveChanges();
                }
            }

            return users;
        }
    }
}