﻿using Doctors.Data;
using Doctors.Models.Appointments;
using Doctors.ViewModels.AppointmentViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctors.Controllers
{
    [Authorize]
    public class AppointmentController : Controller
    {
        private readonly ApplicationDbContext applicationDbContext;

        public AppointmentController(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }

        public IActionResult Index()
        {
            AppointmentsListViewModel model = new AppointmentsListViewModel();

            model.Appointments = applicationDbContext.Appointments.Where(x => x.IsBooked == false);


            return View(model);
        }

        public IActionResult Book(string date, string time)
        {
            BookViewModel model = new BookViewModel();

            var doctors = applicationDbContext.Doctors.ToList();
            var patients = applicationDbContext.Patients.ToList();
            var days = applicationDbContext.Appointments.Where(x => x.IsBooked == false).ToList();
            var times = applicationDbContext.Appointments.Where(x => x.IsBooked == false).ToList();

            model.Doctors = new SelectList(doctors, "Id", "Name");
            model.Patients = new SelectList(patients, "Id", "Name");
            model.Days = new SelectList(days, "Id", "Day");
            model.Times = new SelectList(days, "Id", "Time");

            return View(model);
        }


        [HttpPost]
        public IActionResult Book(BookViewModel model)
        {
            var doctors = applicationDbContext.Doctors.ToList();
            var patients = applicationDbContext.Patients.ToList();
            var days = applicationDbContext.Appointments.ToList();
            var times = applicationDbContext.Appointments.ToList();

            model.Doctors = new SelectList(doctors, "Id", "Name");
            model.Patients = new SelectList(patients, "Id", "Name");
            model.Days = new SelectList(days, "Id", "Day");
            model.Times = new SelectList(days, "Id", "Time");

            var selectedDoctor = model.Doctors.Where(x => x.Value == model.DoctorId).First();
            selectedDoctor.Selected = true;

            var selectedPatient = model.Patients.Where(x => x.Value == model.PatientId).First();
            selectedPatient.Selected = true;


            var selectedTime = model.Times.Where(x => x.Value == Convert.ToString(model.TimesId)).First();
            selectedTime.Selected = true;

            var selectedDay = model.Days.Where(x => x.Value == Convert.ToString(model.DayId)).First();
            selectedDay.Selected = true;

            var appointment = applicationDbContext.Appointments.FirstOrDefault(x => x.PatientId == Convert.ToInt32(model.PatientId) && x.DoctorId == Convert.ToInt32(model.PatientId) && x.Day == selectedDay.Text && x.Time == selectedTime.Text);

            if (ModelState.IsValid)
            {
                if (appointment != null)
                {
                    ModelState.AddModelError("", "Appointment already exists");
                    return View( model);
                }

                Appointment newAppointment = new Appointment();

                newAppointment.DoctorId = Convert.ToInt32(selectedDoctor.Value);
                newAppointment.PatientId = Convert.ToInt32(selectedPatient.Value);
                newAppointment.Time = selectedTime.Text;
                newAppointment.Day = selectedDay.Text;
                newAppointment.IsBooked = true;
                

                applicationDbContext.Appointments.Add(newAppointment);
                applicationDbContext.SaveChanges();

                model.Success = true;

            }

            return View(model);
        }
    }
}
