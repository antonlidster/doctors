﻿using Doctors.Data;
using Doctors.Models.Doctors;
using Doctors.ViewModels.DoctorViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Doctors.Controllers
{
    [Authorize]
    public class DoctorController : Controller
    {
        private readonly ApplicationDbContext applicationDbContext;
        public DoctorController(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }

        public ActionResult Index()
        {
            DoctorsListViewModel model = new DoctorsListViewModel();

            var user = User.Identity.Name;

            var loggedInUser = applicationDbContext.Users.FirstOrDefault(x => x.UserName == user);

            var doctor = applicationDbContext.Doctors.FirstOrDefault(x => x.UserId == loggedInUser.Id);

            model.Patients = applicationDbContext.Patients.Where(x => x.DoctorId == doctor.Id).ToList();

            return View(model);
        }

        public IActionResult Add()
        {
            AddDoctorViewModel model = new AddDoctorViewModel();

            return View(model);
        }

        [HttpPost]
        public IActionResult Add(AddDoctorViewModel model)
        {
            if (ModelState.IsValid)
            {
                var exist = applicationDbContext.Doctors.FirstOrDefault(x => x.Name == model.Name);

                if (exist != null)
                {
                    ModelState.AddModelError("", "Sorry, Doctor already exists.");
                    return View(model);
                }

                Doctor doctor = new Doctor();

                doctor.Name = model.Name;
                doctor.Address = model.Address;
                doctor.Phone = model.Phone;

                applicationDbContext.Doctors.Add(doctor);
                applicationDbContext.SaveChanges();
            }
            else
            {
                ModelState.AddModelError("", "Form has errors");
                return View(model);
            }

            return View(model);
        }
        
    }


}

