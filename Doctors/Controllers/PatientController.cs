﻿using Doctors.Data;
using Doctors.Models.Patients;
using Doctors.ViewModels.PatientViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;

namespace Doctors.Controllers
{
    [Authorize]
    public class PatientController : Controller
    {
        private readonly ApplicationDbContext applicationDbContext;

        public PatientController(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }


        public ActionResult Index()
        {
            PatientsListViewModel model = new PatientsListViewModel();

            model.Patients = applicationDbContext.Patients.ToList();

            return View(model);
        }


        public IActionResult Add()
        {
            AddPatientViewModel model = new AddPatientViewModel();

            var doctors = applicationDbContext.Doctors.ToList();
            model.Doctors = new SelectList(doctors, "Id", "Name");


            return View(model);
        }

        [HttpPost]
        public IActionResult Add(AddPatientViewModel model)
        {
            if (ModelState.IsValid)
            {
                var doctors = applicationDbContext.Doctors.ToList();
                model.Doctors = new SelectList(doctors, "Id", "Name");

                var exist = applicationDbContext.Patients.FirstOrDefault(x => x.Name == model.Name);

                if (exist != null)
                {
                    ModelState.AddModelError("", "Sorry, Doctor already exists.");
                    return View(model);
                }

                Patient patient = new Patient();

                patient.Age = model.Age;
                patient.Gender = Convert.ToChar(model.Gender);
                patient.Name = model.Name;
                patient.Address = model.Address;
                patient.Phone = model.Phone;
                patient.DoctorId = Convert.ToInt32(model.DoctorId);

                applicationDbContext.Patients.Add(patient);
                applicationDbContext.SaveChanges();
            }
            else
            {
                ModelState.AddModelError("", "Form has errors");
                return View(model);
            }

            return View(model);
        }
    }
}
