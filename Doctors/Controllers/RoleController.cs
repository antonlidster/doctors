﻿using Doctors.Data;
using Doctors.Models.Roles;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MVC.ViewModels.RoleViewModels;
using System.Linq;

namespace MVC.Controllers
{
    [Authorize]
    public class RoleController : Controller
    {
        private ApplicationDbContext _context;
        public RoleController(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
        }

        public IActionResult Index(RoleListViewModel model, bool? success)
        {
            model.Roles = _context.Roles.AsEnumerable();
            model.Success = success;

            return View("Index", model);
        }

        public IActionResult Add()
        {
            AddRoleViewModel model = new AddRoleViewModel();
            model.Success = false;
            return View("Add", model);
        }

        [HttpPost]
        public IActionResult Add(AddRoleViewModel model)
        {

            var exists = _context.Roles.FirstOrDefault(x => x.Name == model.Name);

            if (ModelState.IsValid)
            {
                model.Success = false;
                if (exists != null)
                {
                    ModelState.AddModelError("", "Role already exists");
                    return View("Add", model);
                }
                else
                {
                    Role role = new Role();

                    role.Name = model.Name?.Trim();
                    role.NormalizedName = model.Name?.Trim().ToUpper();

                    model.Success = true;

                    _context.Roles.Add(role);
                    _context.SaveChanges();
                }

                return View("Add", model);
            }


            return View("Add", model);
        }


        public IActionResult Edit(string id)
        {
            EditRoleViewModel model = new EditRoleViewModel();
            model.Success = false;

            var role = _context.Roles.FirstOrDefault(x => x.Id == id);

            if (ModelState.IsValid)
            {
                if (role == null)
                {
                    ModelState.AddModelError("", "Role does not exist");
                    return View("Edit", model);
                }

                model.Name = role.Name;

                return View("Edit", model);
            }

            return View("Edit", model);
        }

        [HttpPost]
        public IActionResult Edit(EditRoleViewModel model, string id)
        {
            var role = _context.Roles.FirstOrDefault(x => x.Id == id);

            if (ModelState.IsValid)
            {
                if (role == null)
                {
                    ModelState.AddModelError("", "Role does not exist");
                    return View("Edit", model);
                }

                model.Success = true;

                role.Name = model.Name;

                _context.Roles.Update(role);
                _context.SaveChanges();

                return View("Edit", model);

            }
            return View("Edit", model);
        }

        public IActionResult Delete(string name)
        {
            var role = _context.Roles.FirstOrDefault(x => x.Name == name);

            if (ModelState.IsValid)
            {
                if (role == null)
                {
                    ModelState.AddModelError("", "Role does not exist");
                    return RedirectToAction("Index");
                }

                _context.Roles.Remove(role);
                _context.SaveChanges();

                return RedirectToAction("Index", new { success = true });
            }


            return RedirectToAction("Index");
        }

    }
}
