﻿using Doctors.Data;
using Doctors.ViewModels.UserRoleViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using MVC.DTOs;
using MVC.Models.UserRoles;
using System.Linq;

namespace MVC.Controllers
{
    [Authorize]
    public class UserRoleController : Controller
    {
        private ApplicationDbContext _context;
        public UserRoleController(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
        }

        public IActionResult Index()
        {

            UserRolesListViewModel model = new UserRolesListViewModel();

            var userRoles = _context.UserRoles;

            if (userRoles != null)
            {
                var query = (from userRole in _context.UserRoles
                             from user in _context.Users
                             from role in _context.Roles
                             where user.Id == userRole.UserId && role.Id == userRole.RoleId
                             select new UserRoleDto() { UserName = user.UserName, RoleName = role.Name })?.ToList();


                model.UserRoles = query;

            }

            return View("Index", model);
        }


        public IActionResult Add(AddUserRoleViewModel model)
        {

            model.Success = false;

            var users = _context.Users.ToList();
            model.Users = new SelectList(users, "Id", "UserName");

            var roles = _context.Roles.ToList();
            model.Roles = new SelectList(roles, "Id", "Name");

            return View("Add", model);
        }

        [HttpPost]
        public IActionResult Add(string userId, string roleId, AddUserRoleViewModel model)
        {

            var users = _context.Users.ToList();
            model.Users = new SelectList(users, "Id", "UserName");

            var roles = _context.Roles.ToList();
            model.Roles = new SelectList(roles, "Id", "Name");

            if (ModelState.IsValid)
            {

                var exists = _context.UserRoles.FirstOrDefault(x => x.UserId == userId && x.RoleId == roleId);

                if (exists != null)
                {
                    ModelState.AddModelError("", "User role already exists");
                    return View("Add", model);
                }
                else
                {
                    UserRole userRole = new UserRole();

                    userRole.UserId = userId;
                    userRole.RoleId = roleId;

                    _context.UserRoles.Add(userRole);

                    model.Success = true;


                }

                return View("Add", model);
            }


            return View("Add", model);
        }

        public IActionResult Edit(string username, string rolename)
        {
            EditUserRoleViewModel model = new EditUserRoleViewModel();

            model.Success = false;

            var users = _context.Users.ToList();
            model.Users = new SelectList(users, "Id", "UserName");

            var roles = _context.Roles.ToList();
            model.Roles = new SelectList(roles, "Id", "Name");


            return View("Edit", model);
        }

    }
}
