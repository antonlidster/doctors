﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Doctors.Data;
using Doctors.Models.Users;
using Doctors.ViewModels;
using Doctors.ViewModels.DoctorToPatientViewModels;
using Doctors.ViewModels.UserViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Doctors.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly ApplicationDbContext applicationDbContext;

        public UserController(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            UserListViewModel model = new UserListViewModel();

            model.Users = applicationDbContext.Users.ToList();

            return View(model);
        }

        public IActionResult AssignPatientToDoctor()
        {
            AssignDoctorsToPatientsViewModel model = new AssignDoctorsToPatientsViewModel();
            var doctors = applicationDbContext.Doctors.ToList();
            var patients = applicationDbContext.Patients.ToList();
            model.Doctors = new SelectList(doctors, "Id", "Name");
            model.Patients = new SelectList(patients, "Id", "Name");

            return View(model);
        }

        [HttpPost]
        public IActionResult AssignPatientToDoctor(AssignDoctorsToPatientsViewModel model)
        {
            var doctors = applicationDbContext.Doctors.ToList();
            var patients = applicationDbContext.Patients.ToList();
            model.Doctors = new SelectList(doctors, "Id", "Name");
            model.Patients = new SelectList(patients, "Id", "Name");

            var selectedDoctor = model.Doctors.Where(x => x.Value == model.DoctorId).First();
            selectedDoctor.Selected = true;

            var selectedPatient = model.Patients.Where(x => x.Value == model.PatientId).First();
            selectedPatient.Selected = true;

            if (ModelState.IsValid)
            {
                var exists = applicationDbContext.Patients.FirstOrDefault(x => x.Id == Convert.ToInt32(selectedPatient.Value) && x.DoctorId == Convert.ToInt32(selectedDoctor.Value));

                if (exists != null)
                {
                    ModelState.AddModelError("", "Sorry, Assignment already exists.");
                    return View(model);
                }

                var patient = applicationDbContext.Patients.FirstOrDefault(x => x.Id == Convert.ToInt32(selectedPatient.Value));

                patient.DoctorId = Convert.ToInt32(selectedDoctor.Value);

                applicationDbContext.Update(patient);
                applicationDbContext.SaveChanges();
            }
            return View(model);
        }

        public IActionResult Add()
        {
            AddUserViewModel model = new AddUserViewModel();
            return View(model);
        }
        [HttpPost]
        public IActionResult Add(AddUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var exist = applicationDbContext.Users.FirstOrDefault(x => x.Email == model.Email);
                if (exist != null)
                {
                    ModelState.AddModelError("", "Sorry, User already exists.");
                    return View(model);
                }

                PasswordHasher<User> hasher = new PasswordHasher<User>();
                User user = new User();
                user.Email = model.Email;
                user.PasswordHash = hasher.HashPassword(user, model.PasswordHash);
                user.PhoneNumber = model.PhoneNumber;
                user.UserName = model.UserName;

                //saves user in the database
                applicationDbContext.Add(user);
                applicationDbContext.SaveChanges();

            }
            else
            {
                ModelState.AddModelError("", "Form has errors");
                return View(model);
            }

            return View(model);
        }

        public IActionResult Edit(string username)
        {
            EditUserViewModel model = new EditUserViewModel();

            var user = applicationDbContext.Users.FirstOrDefault(x => x.UserName == username);
            if (user == null)
            {
                ModelState.AddModelError("", "Sorry, User does not exist.");
                return View(model);
            }

            model.UserName = user.UserName;
            model.Email = user.Email;
            model.PhoneNumber = user.PhoneNumber;
            model.UserName = user.UserName;

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(EditUserViewModel model)
        {
            var user = applicationDbContext.Users.FirstOrDefault(x => x.UserName == model.UserName);
            if (user == null)
            {
                ModelState.AddModelError("", "Sorry, User does not exist.");
                return View(model);
            }

            user.UserName = model.UserName;
            user.Email = model.Email;
            user.PhoneNumber = model.PhoneNumber;

            //saves user in the database
            applicationDbContext.Update(user);
            applicationDbContext.SaveChanges();

            return View(model);
        }
    }
}
