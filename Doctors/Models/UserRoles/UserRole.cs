﻿using Doctors.Models.Users;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel;

namespace MVC.Models.UserRoles
{
    public class UserRole : IdentityUserRole<string>
    {
        [DisplayName("UserId")]
        public User User { get; set; }
    }
}
