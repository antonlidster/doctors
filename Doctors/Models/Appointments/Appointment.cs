﻿using Doctors.Models.Doctors;
using Doctors.Models.Patients;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Doctors.Models.Appointments
{
    public class Appointment
    {

        [Key]
        public int Id { get; set; }

        public string Day { get; set; }

        public int? PatientId { get; set; }
        [ForeignKey("PatientId")]
        public Patient Patients { get; set; }

        public int? DoctorId { get; set; }
        [ForeignKey("DoctorId")]
        public Doctor Doctors { get; set; }

        public string Time { get; set; }

        public bool IsBooked { get; set; }

    }
}
