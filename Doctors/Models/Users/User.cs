﻿using Doctors.Models.Doctors;
using Doctors.Models.Patients;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctors.Models.Users
{
    public class User: IdentityUser
    {
        ICollection<Doctor> Doctors { get; set; }
        ICollection<Patient> Patients { get; set; }
    }
}
