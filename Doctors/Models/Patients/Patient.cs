﻿using Doctors.Models.Doctors;
using Doctors.Models.Prescriptions;
using Doctors.Models.Users;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Doctors.Models.Patients
{
    public class Patient
    {

        [Key]
        public int Id { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public string Age { get; set; }

        public char Gender { get; set; }

        public int DoctorId { get; set; }
        [ForeignKey("DoctorId")]
        public Doctor Doctor { get; set; }

        ICollection<Prescription> Prescriptions { get; set; }
    }
}
