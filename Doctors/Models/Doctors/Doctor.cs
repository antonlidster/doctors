﻿using Doctors.Models.Prescriptions;
using Doctors.Models.Users;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Doctors.Models.Doctors
{
    public class Doctor
    {
        [Key]
        public int Id { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        ICollection<Prescription> Prescriptions { get; set; }

    }
}
