﻿using Doctors.Models.Doctors;
using Doctors.Models.Patients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Doctors.Models.Prescriptions
{
    public class Prescription
    {

        [Key]
        public int Id { get; set; }


        public int PatientId { get; set; }
        [ForeignKey("PatientId")]
        public Patient Patients { get; set; }

        public int DoctorId { get; set; }
        [ForeignKey("DoctorId")]
        public Doctor Doctors { get; set; }

        public string Medicine { get; set; }

        public int Quantity { get; set; }

        public String Dosage { get; set; }
    }
}
